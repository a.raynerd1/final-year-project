DROP TABLE comboEmotions;
DROP TABLE words;

CREATE TABLE
IF NOT EXISTS emotions
(date TEXT, emotions TEXT);

CREATE TABLE
IF NOT EXISTS dates
(date TEXT, textMorning TEXT, textAfternoon TEXT, textEvening TEXT);

CREATE TABLE
IF NOT EXISTS comboEmotions
(emotion TEXT, colour TEXT, comboEmotion1 TEXT, comboEmotionColour1 TEXT, comboEmotion2 TEXT, comboEmotionColour2 TEXT, comboEmotion3 TEXT, comboEmotionColour3 TEXT, comboEmotion4 TEXT, comboEmotionColour4 TEXT, comboEmotion5 TEXT, comboEmotionColour5 TEXT, comboEmotion6 TEXT, comboEmotionColour6 TEXT, comboEmotion7 TEXT, comboEmotionColour7 TEXT, comboEmotion8 TEXT, comboEmotionColour8 TEXT);

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'joy', '#fbee7f', 'none', 'none', 'love', '#C8D23F' ,'guilt', '#93BE4F', 'delight', '#98BD86', 'none', 'none', 'morbidness', '#CAA881', 'pride', '#E5824F', 'optimism', '#F2BA47'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='joy');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'trust', '#c5dd63', 'love', '#C8D23E', 'none', 'none', 'submission', '#65B744' ,'curiosity', '#6DB77A', 'sentimental', '#86B37C', 'none', 'none', 'dominance', '#B97E45', 'hope', '#C5B135'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='trust');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'fear', '#78bd74', 'guilt', '#95BE4C', 'submission', '#67B740', 'none', 'none', 'awe', '#37A488' ,'despair', '#539F89', 'shame', '#698E82', 'none', 'none', 'anxiety', '#929D43'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='fear');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'surprise', '#8bc5d5', 'delight', '#9ABF87', 'curiosity', '#6CB87B', 'awe', '#39A487', 'none', 'none', 'disapproval', '#58A0C4' ,'disbelief', '#6E8EBD', 'outrage', '#8A6A8C', 'none', 'none'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='surprise');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'sadness', '#a4bcd5', 'none', 'none', 'sentimental', '#86B37C', 'despair', '#539F89', 'disapproval', '#57A0C5', 'none', 'none', 'remorse', '#8889BE' ,'envy', '#A9668C', 'pessimism', '#B2997F'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='sadness');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'disgust', '#b89ac7', 'morbidness', '#CAA980', 'none', 'none', 'shame', '#6A8E7E', 'disbelief', '#6C8CB9', 'remorse', '#898ABB', 'none', 'none', 'contempt', '#BA5688' ,'cynicism', '#C88874'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='disgust');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'anger', '#ec636a', 'pride', '#E68553', 'dominance', '#B87E47', 'none', 'none', 'outrage', '#8C698D', 'envy', '#A46690', 'contempt', '#BA5488', 'none', 'none', 'aggressiveness', '#E46449'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='anger');

INSERT INTO comboEmotions(emotion, colour, comboEmotion1, comboEmotionColour1, comboEmotion2, comboEmotionColour2, comboEmotion3, comboEmotionColour3, comboEmotion4, comboEmotionColour4, comboEmotion5, comboEmotionColour5, comboEmotion6, comboEmotionColour6, comboEmotion7, comboEmotionColour7, comboEmotion8, comboEmotionColour8)
SELECT 'anticipation', '#f8b968', 'optimism', '#F4B842', 'hope', '#C6B136', 'anxiety', '#939D42', 'none', 'none', 'pessimism', '#B2997F', 'cynicism', '#C88777', 'aggressiveness', '#E26246', 'none', 'none'
WHERE NOT EXISTS (SELECT * FROM comboEmotions WHERE emotion='anticipation');

CREATE TABLE
IF NOT EXISTS words
(associatedEmotion TEXT, word TEXT, severity TEXT);

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('joy', 'joy', '2' ), ('joy', 'overjoyed', '3' ), ('joy', 'ecstatic', '3' ), ('joy', 'happy', '2'), ('joy', 'serene', '1'), ('joy', 'glad', '1'), ('joy', 'good', '1'), ('joy', 'content', '1'), ('joy', 'satisfied', '1'), ('joy', 'gratified', '1'), ('joy', 'pleasant', '1'), ('joy', 'fine', '1'), ('joy', 'thrilled', '3' ), ('joy', 'cloud nine', '3' ), ('joy', 'overjoyed', '3' ), ('joy', 'excited', '3' ), ('joy', 'elated', '3' ), ('joy', 'sensational', '3' ), ('joy', 'exhilarated', '3' ), ('joy', 'fantastic', '3' ), ('joy', 'on top', '3' ), ('joy', 'euphoric', '3' ), ('joy', 'enthusiastic', '3' ), ('joy', 'delighted', '3' ), ('joy', 'marvelous', '3' ), ('joy', 'great', '3' ), ('joy', 'cheerful', '2'), ('joy', 'lighthearted', '2'), ('joy', 'serene', '2'), ('joy', 'wonderful', '2'), ('joy', 'aglow', '2'), ('joy', 'glowing', '2'), ('joy', 'in high spirits', '2'), ('joy', 'jovial', '2'), ('joy', 'high', '2'), ('joy', 'elevated', '2'), ('joy', 'neat', '2'))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('trust', 'trust', '2' ), ('trust', 'admire', '3'), ('trust', 'accept', '1'), ('trust', 'confident', '3'), ('trust', 'expect', '2'), ('trust', 'faith', '3'), ('trust', 'hope', '2'), ('trust', 'assured', '2'), ('trust', 'certain', '3'), ('trust', 'conviction', '3'), ('trust', 'dependent', '1'), ('trust', 'positive', '2'), ('trust', 'rely', '2' ), ('trust', 'stock', '1' ), ('trust', 'sure', '2' ), ('trust', 'entrust', '2' ), ('trust', 'confide', '1' ), ('trust', 'recommend', '1' ), ('trust', 'empower', '2' ))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('fear', 'fear', '2' ), ('fear', 'terrified', '3'), ('fear', 'apprehensive', '1'), ('fear', 'frightened', '3'), ('fear', 'intimidated', '3'), ('fear', 'horrified', '3'), ('fear', 'desperate', '3'), ('fear', 'panic', '3'), ('fear', 'stagefright', '3'), ('fear', 'vulnerable', '3'), ('fear', 'dread', '3' ), ('fear', 'paralyzed', '3' ), ('fear', 'afraid', '2' ), ('fear', 'scared', '2' ), ('fear', 'shaky', '2' ), ('fear', 'threatened', '2' ), ('fear', 'distrustful', '2' ), ('fear', 'risky', '2' ), ('fear', 'alarmed', '2' ), ('fear', 'awkward', '2' ), ('fear', 'defensive', '2' ), ('fear', 'jumpy', '2' ), ('fear', 'self conscious', '1' ), ('fear', 'on edge', '1' ), ('fear', 'jittery', '1' ), ('fear', 'nervous', '1' ), ('fear', 'anxious', '1' ), ('fear', 'hesitent', '1' ), ('fear', 'timid', '1' ), ('fear', 'shy', '1' ), ('fear', 'worried', '1' ), ('fear', 'uneasy', '1' ), ('fear', 'bashful', '1' ), ('fear', 'embarrased', '1' ))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('surprise', 'surprise', '2' ), ('surprised', 'astonish', '3'), ('surprise', 'amaze', '2'), ('surprise', 'startle', '1'), ('surprise', 'flabbergast', '3'), ('surprise', 'astound', '2'), ('surprise', 'stagger', '3'), ('surprise', 'shock', '2'), ('surprise', 'stupefied', '3'), ('surprise', 'dumbfound', '3'), ('surprise', 'daze', '2' ), ('surprise', 'startle', '1' ), ('surprise', 'stun', '1' ), ('surprise', 'stagger', '2' ), ('surprise', 'awe', '3' ), ('surprise', 'bewilder', '2' ), ('surprise', 'wonder', '1' ), ('surprise', 'jolt', '1' ), ('surprise', 'thunderstruck', '3' ), ('surprise', 'speechless', '2' ))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('sadness', 'sadness', '2' ), ('sadness', 'unhappy', '2'), ('sadness', 'dejected', '3'), ('sadness', 'regret', '1'), ('sadness', 'depressed', '3'), ('sadness', 'miserable', '3'), ('sadness', 'despondent', '3'), ('sadness', 'despair', '2'), ('sadness', 'desolate', '3'), ('sadness', 'cheerless', '2'), ('sadness', 'downhearted', '2' ), ('sadness', 'wretched', '2' ), ('sadness', 'glum', '1' ), ('sadness', 'gloomy', '1' ), ('sadness', 'melancholy', '1' ), ('sadness', 'mournful', '2' ), ('sadness', 'brokenhearted', '3' ), ('sadness', 'heartache', '2' ), ('sadness', 'grief', '2' ), ('sadness', 'woe', '2' ), ('sadness', 'down', '1' ), ('sadness', 'dismal', '2' ), ('sadness', 'woe', '2' ), ('sadness', 'anguish', '3' ), ('sadness', 'hopeless', '2' ), ('sadness', 'mourning', '3' ), ('sadness', 'listless', '2' ), ('sadness', 'moping', '1' ), ('sadness', 'letdown', '1' ))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('disgust', 'disgust', '2' ), ('disgust', 'disgusted', '2'), ('disgust', 'revolted', '2'), ('disgust', 'sickened', '3'), ('disgust', 'gross', '2'), ('disgust', 'outraged', '2'), ('disgust', 'outrage', '2'), ('disgust', 'shudder', '2'), ('disgust', 'shuddered', '2'), ('disgust', 'repugnant', '3'), ('disgust', 'horrified', '2' ), ('disgust', 'appalled', '2' ), ('disgust', 'offended', '1' ), ('disgust', 'affront', '1' ), ('disgust', 'dismayed', '1' ), ('disgust', 'dismay', '1' ), ('disgust', 'displeased', '1' ), ('disgust', 'annoyed', '1' ), ('disgust', 'revulsed', '3' ), ('disgust', 'loath', '3' ), ('disgust', 'detest', '3' ), ('disgust', 'yuck', '1' ), ('disgust', 'repelled', '2' ))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('anger', 'anger', '2' ), ('anger', 'angry', '2'), ('anger', 'annoyed', '2'), ('anger', 'vexed', '1'), ('anger', 'irritated', '3'), ('anger', 'irritable', '1'), ('anger', 'exasperation', '1'), ('anger', 'indignation', '2'), ('anger', 'indignant', '2'), ('anger', 'rage', '3'), ('anger', 'fury', '3' ), ('anger', 'furious', '3' ), ('anger', 'wrath', '3' ), ('anger', 'outraged', '2' ), ('anger', 'temper', '2' ), ('anger', 'ire', '2' ), ('anger', 'enraged', '3' ), ('anger', 'riled', '2' ), ('anger', 'antagonised', '3' ), ('anger', 'hate', '3' ), ('anger', 'hatred', '3' ))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );

INSERT INTO words (associatedEmotion, word, severity)
WITH newWords(associatedEmotion, word, severity) AS (VALUES ('anticipation', 'anticipation', '2' ), ('anticipation', 'anticipate', '2'), ('anticipation', 'apprehension', '2'), ('anticipation', 'anxiety', '3'), ('anticipation', 'hope', '3'), ('anticipation', 'trust', '1'), ('anticipation', 'impatient', '2'))
SELECT *
FROM  newWords
WHERE  NOT EXISTS ( SELECT 1
                     FROM   words AS MT
                     WHERE  MT.associatedEmotion = newWords.associatedEmotion );