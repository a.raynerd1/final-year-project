import { Component } from '@angular/core';
import { DatabaseService, Emotions } from './../services/database.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TreeMapComponent } from './../components/tree-map/tree-map.component';
import { StackedAreaChartComponent } from './../components/stacked-area-chart/stacked-area-chart.component'


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  options: Array<string> = [
    'Moods by Category', 'Moods by Word', 'Mood By Day Of The Week', 'Mood Over The Month', 'Strongest Emotions'
  ];
  currentOption: number = 0;
  constructor(
    private db: DatabaseService
  ) {

  }
  changeOption(choice: string) {
    if (choice = "next") {
      if (this.currentOption == 4) {
        this.currentOption = 0;
      } else {
        this.currentOption++;
      }
    } else {
      if (this.currentOption == 0) {
        this.currentOption = 4;
      } else {
        this.currentOption--;
      }
    }
  }
}
