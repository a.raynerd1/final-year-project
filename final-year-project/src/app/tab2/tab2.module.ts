import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { Component } from '@angular/core';
import * as moment from 'moment';
import { DatabaseService, Emotions } from './../services/database.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TreeMapComponent } from './../components/tree-map/tree-map.component';
import { StackedAreaChartComponent } from './../components/stacked-area-chart/stacked-area-chart.component'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab2Page }]),
    NgxChartsModule
  ],
  declarations: [Tab2Page, TreeMapComponent, StackedAreaChartComponent]
})
export class Tab2PageModule {
  arr: String;
  constructor(
    private db: DatabaseService
  ) {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        // this.arr = this.db.loadEmotions();
      }
    });
  }
}
