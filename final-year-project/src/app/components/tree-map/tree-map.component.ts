import { Component, NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DatabaseService, Emotions } from './../../services/database.service';

@Component({
  selector: 'app-tree-map',
  templateUrl: './tree-map.component.html',
  styleUrls: ['./tree-map.component.css']
})
export class TreeMapComponent {
  moodCounts: any[];
  view: any[] = [window.outerWidth, window.outerHeight - 200];

  // options
  gradient: boolean = false;
  animations: boolean = true;
  alreadyRun: boolean = false;
  counts: Array<number> = [0, 0, 0, 0, 0, 0, 0, 0];
  colorScheme = {
    domain: ['#fbee7f', '#c5dd63', '#78bd74', '#8bc5d5', '#a4bcd5', '#b89ac7', '#ec636a', '#f8b968']
  };



  constructor(
    private db: DatabaseService
  ) {
    let moodCounts = [
      {
        "name": "Joy",
        "value": 0
      },
      {
        "name": "Trust",
        "value": 0
      },
      {
        "name": "Fear",
        "value": 0
      },
      {
        "name": "Surprise",
        "value": 0
      },
      {
        "name": "Sadness",
        "value": 0
      }, {
        "name": "Disgust",
        "value": 0
      }, {
        "name": "Anger",
        "value": 0
      }, {
        "name": "Anticipation",
        "value": 0
      }
    ];
    Object.assign(this, { moodCounts });
    this.setMoodCounts();
  }

  getMoodCounts(res: any) {

    for (let i = 0; i < res.length; i++) {
      if (res[i].associatedEmotion == "joy") {
        this.counts[0]++;
      }
      if (res[i].associatedEmotion == "trust") {
        this.counts[1]++;
      }
      if (res[i].associatedEmotion == "fear") {
        this.counts[2]++;
      }
      if (res[i].associatedEmotion == "surprise") {
        this.counts[3]++;
      }
      if (res[i].associatedEmotion == "sadness") {
        this.counts[4]++;
      }
      if (res[i].associatedEmotion == "disgust") {
        this.counts[5]++;
      }
      if (res[i].associatedEmotion == "anger") {
        this.counts[6]++;
      }
      if (res[i].associatedEmotion == "anticipation") {
        this.counts[7]++
      }
    }
  }

  setMoodCounts() {
    if (this.alreadyRun == false) {
      this.alreadyRun = true;
      let stringBuild = '';
      let stringArrays = [];
      this.db.getDiaries().then(res => {
        for (let i = 0; i < res.length; i++) {
          if (res[i].textMorning) {
            let textMorningArray = [];
            res[i].textMorning = res[i].textMorning.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()']/g, "")
            textMorningArray = res[i].textMorning.split(' ');
            for (let j = 0; j < textMorningArray.length; j++) {
              stringBuild += "'" + textMorningArray[j] + "'" + " OR word = ";
              if (stringBuild.length > 800) {
                stringArrays.push(stringBuild);
                stringBuild = '';
              }
            }
          }
          if (res[i].textAfternoon) {
            let textAfternoonArray = [];
            res[i].textAfternoon = res[i].textAfternoon.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()']/g, "")
            textAfternoonArray = res[i].textAfternoon.split(' ');
            for (let j = 0; j < textAfternoonArray.length; j++) {
              stringBuild += "'" + textAfternoonArray[j] + "'" + " OR word = ";
              if (stringBuild.length > 800) {
                stringArrays.push(stringBuild);
                stringBuild = '';
              }
            }
          }
          if (res[i].textEvening) {
            let textEveningArray = [];
            res[i].textEvening = res[i].textEvening.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()']/g, "")
            textEveningArray = res[i].textEvening.split(' ');
            for (let j = 0; j < textEveningArray.length; j++) {
              stringBuild += "'" + textEveningArray[j] + "'" + " OR word = ";
              if (stringBuild.length > 800) {
                stringArrays.push(stringBuild);
                stringBuild = '';
              }
            }
          }
        }
        let arr = [];
        for (let i = 0; i < stringArrays.length; i++) {
          this.db.getDiaryDates(stringArrays[i]).then(res => {
            for (let j = 0; j < res.length; j++) {
              arr.push(res[j]);
            }
          });
        }
        setTimeout(() => {
          this.getMoodCounts(arr);
          this.moodCounts = [
            {
              "name": "Joy",
              "value": this.counts[0]
            },
            {
              "name": "Trust",
              "value": this.counts[1]
            },
            {
              "name": "Fear",
              "value": this.counts[2]
            },
            {
              "name": "Surprise",
              "value": this.counts[3]
            },
            {
              "name": "Sadness",
              "value": this.counts[4]
            }, {
              "name": "Disgust",
              "value": this.counts[5]
            }, {
              "name": "Anger",
              "value": this.counts[6]
            }, {
              "name": "Anticipation",
              "value": this.counts[7]
            }
          ];
        }, 1000);
      });
    }
  }

  onSelect(event) {
    console.log(event);
  }

  labelFormatting(c) {
    return `${(c.label)}`;
  }
}
