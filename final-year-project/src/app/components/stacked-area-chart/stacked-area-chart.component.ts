import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DatabaseService, Emotions } from './../../services/database.service';

@Component({
    selector: 'app-stacked-area-chart',
    templateUrl: './stacked-area-chart.component.html',
    styleUrls: ['./stacked-area-chart.component.css']
})
export class StackedAreaChartComponent {

    moodCounts: any[];
    view: any[] = [window.outerWidth, window.outerHeight - 300];
    alreadyRun: boolean = false;
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    // options
    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Date Of Month';
    yAxisLabel: string = 'Number of Associated Words in Diary';
    timeline: boolean = true;
    months = ['September 2019', 'October 2019', 'November 2019', 'December 2019', 'January 2020', 'February 2020', 'March 2020', 'April 2020', 'May 2020', 'June 2020', 'July 2020'];
    currentlySelected = 8;

    colorScheme = {
        domain: ['#fbee7f', '#c5dd63', '#78bd74', '#8bc5d5', '#a4bcd5', '#b89ac7', '#ec636a', '#f8b968']
    };

    constructor(
        private db: DatabaseService
    ) {
        let moodCounts = [
            {
                "name": "Joy",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },

            {
                "name": "Trust",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },

            {
                "name": "Fear",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },
            {
                "name": "Surprise",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },
            {
                "name": "Sadness",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },

            {
                "name": "Disgust",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },

            {
                "name": "Anger",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            },
            {
                "name": "Anticipation",
                "series": [
                    {
                        "name": "1-10",
                        "value": 0
                    },
                    {
                        "name": "11-20",
                        "value": 0
                    },
                    {
                        "name": "21-",
                        "value": 0
                    }
                ]
            }
        ];
        Object.assign(this, { moodCounts });
        this.setMoodCounts();
    }

    changeMonth(change) {
        if (change == "next") {
            if (this.currentlySelected == 10) {
                this.currentlySelected = 0;
            } else {
                this.currentlySelected++;
            }
        } else {
            if (this.currentlySelected == 0) {
                this.currentlySelected = 10;
            } else {
                this.currentlySelected--;
            }
        }
        this.counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.setMoodCounts();
    }

    getPartOfMonth(res): number {
        let number = res.date.split(" ")[1].replace(",", "");
        if (number < 11) {
            return 1;
        } else if (number < 21) {
            return 2;
        } else {
            return 3;
        }
    }

    getMoodCounts(res: any) {
        for (let i = 0; i < res.length; i++) {
            if ((res[i].object.associatedEmotion == "joy") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[0]++;
            }
            if ((res[i].object.associatedEmotion == "joy") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[1]++;
            }
            if ((res[i].object.associatedEmotion == "joy") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[2]++;
            }
            if ((res[i].object.associatedEmotion == "trust") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[3]++;
            }
            if ((res[i].object.associatedEmotion == "trust") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[4]++;
            }
            if ((res[i].object.associatedEmotion == "trust") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[5]++;
            }
            if ((res[i].object.associatedEmotion == "fear") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[6]++;
            }
            if ((res[i].object.associatedEmotion == "fear") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[7]++;
            }
            if ((res[i].object.associatedEmotion == "fear") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[8]++;
            }
            if ((res[i].object.associatedEmotion == "surprise") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[9]++;
            }
            if ((res[i].object.associatedEmotion == "surprise") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[10]++;
            }
            if ((res[i].object.associatedEmotion == "surprise") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[11]++;
            }
            if ((res[i].object.associatedEmotion == "sadness") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[12]++;
            }
            if ((res[i].object.associatedEmotion == "sadness") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[13]++;
            }
            if ((res[i].object.associatedEmotion == "sadness") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[14]++;
            }
            if ((res[i].object.associatedEmotion == "disgust") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[15]++;
            }
            if ((res[i].object.associatedEmotion == "disgust") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[16]++;
            }
            if ((res[i].object.associatedEmotion == "disgust") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[17]++;
            }
            if ((res[i].object.associatedEmotion == "anger") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[18]++;
            }
            if ((res[i].object.associatedEmotion == "anger") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[19]++;
            }
            if ((res[i].object.associatedEmotion == "anger") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[20]++;
            }
            if ((res[i].object.associatedEmotion == "anticipation") && (this.getPartOfMonth(res[i]) === 1)) {
                this.counts[21]++
            }
            if ((res[i].object.associatedEmotion == "anticipation") && (this.getPartOfMonth(res[i]) === 2)) {
                this.counts[22]++
            }
            if ((res[i].object.associatedEmotion == "anticipation") && (this.getPartOfMonth(res[i]) === 3)) {
                this.counts[23]++
            }
        }
    }

    setMoodCounts() {
        let stringBuild = '';
        let stringArrays = [];
        let dateArray = [];
        this.db.getDiariesForSpecificMonth(this.months[this.currentlySelected]).then(res => {
            for (let i = 0; i < res.length; i++) {
                if (res[i].textMorning) {
                    let textMorningArray = [];
                    res[i].textMorning = res[i].textMorning.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()']/g, "")
                    textMorningArray = res[i].textMorning.split(' ');
                    for (let j = 0; j < textMorningArray.length; j++) {
                        stringBuild += "'" + textMorningArray[j] + "'" + " OR word = ";
                        if (stringBuild.length > 800) {
                            dateArray.push(res[i].date);
                            stringArrays.push(stringBuild);
                            stringBuild = '';
                        }
                    }
                }
                if (res[i].textAfternoon) {
                    let textAfternoonArray = [];
                    res[i].textAfternoon = res[i].textAfternoon.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()']/g, "")
                    textAfternoonArray = res[i].textAfternoon.split(' ');
                    for (let j = 0; j < textAfternoonArray.length; j++) {
                        stringBuild += "'" + textAfternoonArray[j] + "'" + " OR word = ";
                        if (stringBuild.length > 800) {
                            dateArray.push(res[i].date);
                            stringArrays.push(stringBuild);
                            stringBuild = '';
                        }
                    }
                }
                if (res[i].textEvening) {
                    let textEveningArray = [];
                    res[i].textEvening = res[i].textEvening.toLowerCase().replace(/[.,\/#!$%\^&\*;:{}=\-_`~()']/g, "")
                    textEveningArray = res[i].textEvening.split(' ');
                    for (let j = 0; j < textEveningArray.length; j++) {
                        stringBuild += "'" + textEveningArray[j] + "'" + " OR word = ";
                        if (stringBuild.length > 800) {
                            dateArray.push(res[i].date);
                            stringArrays.push(stringBuild);
                            stringBuild = '';
                        }
                    }
                }
                dateArray.push(res[i].date);
                stringArrays.push(stringBuild);
            }
            let arr = [];
            for (let i = 0; i < stringArrays.length; i++) {
                this.db.getDiaryDatesForSpecificMonth(stringArrays[i], dateArray[i]).then(res => {
                    for (let j = 0; j < res.length; j++) {
                        arr.push({
                            object: res[j],
                            date: res[j + 1]
                        });
                        j++;

                    }
                });
            }
            setTimeout(() => {
                this.getMoodCounts(arr);
                this.moodCounts = [
                    {
                        "name": "Joy",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[0]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[1]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[2]
                            }
                        ]
                    },

                    {
                        "name": "Trust",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[3]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[4]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[5]
                            }
                        ]
                    },

                    {
                        "name": "Fear",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[6]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[7]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[8]
                            }
                        ]
                    },
                    {
                        "name": "Surprise",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[9]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[10]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[11]
                            }
                        ]
                    },
                    {
                        "name": "Sadness",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[12]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[13]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[14]
                            }
                        ]
                    },

                    {
                        "name": "Disgust",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[15]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[16]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[17]
                            }
                        ]
                    },

                    {
                        "name": "Anger",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[18]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[19]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[20]
                            }
                        ]
                    },
                    {
                        "name": "Anticipation",
                        "series": [
                            {
                                "name": "1-10",
                                "value": this.counts[21]
                            },
                            {
                                "name": "11-20",
                                "value": this.counts[22]
                            },
                            {
                                "name": "21-",
                                "value": this.counts[23]
                            }
                        ]
                    }
                ];
            }, 1000);
        });
    }

    onSelect(event) {
        console.log(event);
    }
}