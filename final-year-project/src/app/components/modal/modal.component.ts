import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatabaseService, Emotions } from './../../services/database.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  diaryMorning: String;
  diaryAfternoon: String;
  diaryEvening: String;
  date: String;

  constructor(
    public dialogRef: MatDialogRef<ModalComponent>,
    private db: DatabaseService,
    @Inject(MAT_DIALOG_DATA) public modalData: any,
  ) {
    console.log(this.modalData);
    this.db.getDiariesForSingleDate(this.modalData.date).then(res => {
      if (res[0]) {
        this.diaryMorning = res[0].textMorning;
        this.diaryAfternoon = res[0].textAfternoon;
        this.diaryEvening = res[0].textEvening;
      }
    });

  }

  ngOnInit() {
   
   }

  actionFunction() {
    this.closeModal();
  }

  moveToPrevDiary() {

  }

  moveToNextDiary() {

  }

  saveDiaries() {
    this.date = this.modalData.date;
    this.db.saveDiary(this.date, this.diaryMorning, this.diaryAfternoon, this.diaryEvening).then(res => {
      console.log(res);
    });
  }



  closeModal() {
    this.dialogRef.close();
  }

}


