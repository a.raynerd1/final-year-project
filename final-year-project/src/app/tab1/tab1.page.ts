import { Component } from '@angular/core';
import * as moment from 'moment';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogModule, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from '.././components/modal/modal.component';
import { DatabaseService, Emotions } from './../services/database.service';
import { AngularDelegate } from '@ionic/angular';

interface moods {
  [index: number]: { mood: String; hex: String; };
}
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  emotions: Emotions[] = [];
  emotion = {};
  title = 'ng-calendar-demo';
  selectedDate = new Date(new Date().setMonth(new Date().getMonth()));
  startAt = new Date(new Date().setMonth(new Date().getMonth()));
  minDate = new Date('2019/09/14');
  maxDate = new Date(new Date().setMonth(new Date().getMonth() + 1));
  year: any;
  DayAndDate: string;
  currentMonth: string;
  moodColours: Array<moods> = [];
  moods: Array<String> = [];
  hexes: Array<String> = [];
  currentlyClicked: String;
  dateForSearch: String;
  lastClicked: number;
  paletteRun: boolean = false;
  currentDate: string;
  allDaysWithEmotion = [];
  datesForSearch: String[] = [];
  constructor(
    public matDialog: MatDialog,
    private db: DatabaseService
  ) {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getEmotion().subscribe(emotions => {
          this.emotions = emotions;
          this.setupPage();
          this.setupMoodPalette();
          let arr = this.db.getAllEmotions();
          console.log(arr);
          this.getAndSetAllEmotions();
          let words = this.db.getAllWords('fear');
          console.log(words);
        })
      }
    });
    // this.onSelect(this.selectedDate, args);
  }

  addEmotion(): Promise<any> {
    return this.db.getEmotionsForSetDate(this.dateForSearch).then(res => {
      console.log('Promise' + res[0]);
      if (res.length == 0) {
        return this.db.addEmotions(this.dateForSearch, this.currentlyClicked).then(result => {
          this.emotion = {};
          let returned = this.db.getAllEmotions();
          console.log(returned);
          this.currentlyClicked = null;
          this.getAndSetAllEmotions();
          return result;
        });
      } else {
        this.db.deleteEmotion(this.dateForSearch).then(res => {
          return this.db.addEmotions(this.dateForSearch, this.currentlyClicked).then(result => {
            this.emotion = {};
            let returned = this.db.getAllEmotions();
            console.log(returned);
            this.currentlyClicked = null;
            this.getAndSetAllEmotions();
            return result;
          });
        })
      }
    });
  }

  delete() {
    this.db.deleteEmotion(this.emotions['date']).then(() => {
    });
  }

  getAndSetAllEmotions() {
    let allEmotions: Array<Emotions>;
    this.db.loadAndReturnAllEmotions().then(data => {
      console.log(data);
      allEmotions = data;
      for (let i = 0; i < allEmotions.length; i++) {
        let dateToChange = allEmotions[i].date;
        let dateValue = dateToChange.split(' ')[1];
        dateValue = dateValue.slice(0, -1);
        let dateValueInt = parseInt(dateValue);
        dateValueInt -= 1;
        dateValue = dateValueInt.toString();
        let div = document.getElementsByClassName('mat-calendar-body-cell')[dateValue];
        if (div) {
          this.datesForSearch.push(div.getAttribute('aria-label').toString());
          console.log(this.currentDate);
        }
      }
      this.db.getEmotionForMultipleDates(this.datesForSearch).then(res => {
        if (res.length > 0) {
          this.datesForSearch = [];
          this.allDaysWithEmotion = [];
          console.log("Result: " + res[0] + res[1]);
          for (let i = 0; i < res.length; i++) {
            this.allDaysWithEmotion.push(res[i]);
            // allElements[dateValueInt].classList.add();
          }
          let allMoods = [];
          this.db.getEmotionsFromComboEmotions().then(res => {
            for (let i = 0; i < res.length; i++) {
              allMoods.push([{ mood: res[i].emotion, hex: res[i].colour }]);
              if (res[i].comboEmotion1 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion1, hex: res[i].comboEmotionColour1 }]);
              }
              if (res[i].comboEmotion2 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion2, hex: res[i].comboEmotionColour2 }]);
              }
              if (res[i].comboEmotion3 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion3, hex: res[i].comboEmotionColour3 }]);
              }
              if (res[i].comboEmotion4 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion4, hex: res[i].comboEmotionColour4 }]);
              }
              if (res[i].comboEmotion5 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion5, hex: res[i].comboEmotionColour5 }]);
              }
              if (res[i].comboEmotion6 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion6, hex: res[i].comboEmotionColour6 }]);
              }
              if (res[i].comboEmotion7 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion7, hex: res[i].comboEmotionColour7 }]);
              }
              if (res[i].comboEmotion8 !== 'none') {
                allMoods.push([{ mood: res[i].comboEmotion8, hex: res[i].comboEmotionColour8 }]);
              }
            }
            console.log(allMoods);
            for (let i = 0; i < this.allDaysWithEmotion.length; i++) {
              let dateToChange = this.allDaysWithEmotion[i].date;
              let dateValue = dateToChange.split(' ')[1];
              dateValue = dateValue.slice(0, -1);
              let dateValueInt = parseInt(dateValue);
              dateValueInt -= 1;
              dateValue = dateValueInt.toString();
              let div = document.getElementsByClassName('mat-calendar-body-cell')[dateValue];
              if (div) {
                let setHex = "";
                for (let j = 0; j < allMoods.length; j++) {
                  if (allMoods[j][0].mood == this.allDaysWithEmotion[i].emotions) {
                    setHex = allMoods[j][0].hex;
                  }
                }
                console.log('Date: ' + div.getAttribute('aria-label').toString() + 'Mood' + this.allDaysWithEmotion[i].emotions + setHex);
                div.setAttribute('style', "color: " + setHex);
              }
            }
            console.log(this.allDaysWithEmotion);
          });
        } else {
          this.datesForSearch = [];
          console.log("No dates for month");
        }
      });
    });
  }

  openDiaryModal() {
    let contentCells = document.getElementsByClassName('mat-calendar-body-cell');
    let diaryModal = () => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      let height = window.innerHeight - 100;
      let width = window.innerWidth - 100;
      dialogConfig.id = "modal-component";
      dialogConfig.height = height + "px";
      dialogConfig.width = width + "px";
      dialogConfig.data = {
        date: this.dateForSearch
      }
      this.matDialog.open(ModalComponent, dialogConfig);
    }
    for (let i = 0; i < contentCells.length; i++) {
      contentCells[i].addEventListener('touchstart', function (e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        let nowClicked;
        nowClicked = Date.now();
        console.log('NowClicked= ' + nowClicked);
        if (this.lastClicked) {
          console.log('LastClicked= ' + this.lastClicked);
          if (nowClicked - this.lastClicked < 500) {
            console.log('Double clicked!');
            diaryModal();
          }
        }
        this.lastClicked = nowClicked;
      });
    }
  }

  deleteDatabase() {
    this.db.deleteAllEmotions().then(response => {
      console.log(response);
    }).catch(response => {
      console.log(response);
    });
  }

  onSelect(event) {
    console.log(event);
    this.selectedDate = event;
    const dateString = event.toDateString();
    const dateValue = dateString.split(' ');
    this.year = dateValue[3];
    this.DayAndDate = dateValue[0] + ',' + ' ' + dateValue[1] + ' ' + dateValue[2];
    this.dateForSearch = moment(this.selectedDate).format('MMMM D, YYYY');
    console.log(this.dateForSearch);
    if (this.currentlyClicked) {
      this.addEmotion().then(_ => {
        console.log('Emotion added');
      });
    }
  }

  setupPage() {
    setTimeout(() => {
      this.setupNextButton();
      this.setupPrevButton();
      this.deleteElements();
    }, 200)
  }

  setupMoodPalette() {
    if (this.moodColours.length == 0 && this.paletteRun == false) {
      this.paletteRun = true;
      this.db.getEmotionsFromComboEmotions().then(arr => {
        for (let i = 0; i < arr.length; i++) {
          if (this.moodColours.length < 8) {
            console.log('Emotions: ' + arr[i].emotion + ' ' + arr[i].colour);
            this.moodColours.push([{ mood: arr[i].emotion, hex: arr[i].colour }]);
          }
        }
      })
    } else {
      console.log('Already run!');
    }
  }

  setupNextButton() {
    let nextMonthButton: HTMLButtonElement = document.getElementsByClassName("mat-calendar-next-button")[0] as HTMLButtonElement;
    let nextMonth = document.getElementsByClassName('mat-calendar-next-button')[0];
    nextMonth.addEventListener('click', () => {
      this.setupElements();
    });
    nextMonthButton.style.display = "none";
  }

  setupPrevButton() {
    let prevMonthButton: HTMLButtonElement = document.getElementsByClassName("mat-calendar-previous-button")[0] as HTMLButtonElement;
    let prevMonth = document.getElementsByClassName('mat-calendar-previous-button')[0];
    prevMonth.addEventListener('click', () => {
      this.setupElements();
    });
    prevMonthButton.style.display = "none";
  }

  deleteElements() {
    let deleteElements = [];
    deleteElements.push(document.getElementsByClassName('mat-calendar-period-button')[0]);
    for (let i = 0; i < deleteElements.length; i++) {
      if (deleteElements[i] !== undefined) {
        deleteElements[i].remove();
      }

    }
    this.setupElements();
  }

  setupElements() {
    this.currentMonth = document.getElementsByClassName('mat-calendar-body-cell')[0].getAttribute('aria-label').toString();
    let split = this.currentMonth.split(" ");
    this.currentMonth = split[0] + " " + split[2];
    let changeElementsArray = document.getElementsByClassName('mat-calendar-body-cell');
    let changeElementsArrayChildren = [];
    for (let i = 0; i < changeElementsArray.length; i++) {
      changeElementsArrayChildren.push(changeElementsArray[i].firstChild);
    }
  }

  prevMonth() {
    let element: HTMLButtonElement = document.getElementsByClassName("mat-calendar-previous-button")[0] as HTMLButtonElement;
    element.click();
    setTimeout(() => {
      this.currentMonth = document.getElementsByClassName('mat-calendar-body-cell')[0].getAttribute('aria-label').toString();
      let split = this.currentMonth.split(" ");
      this.currentMonth = split[0] + " " + split[2];
      let changeElementsArray = document.getElementsByClassName('mat-calendar-body-cell');
      let changeElementsArrayChildren = [];
      for (let i = 0; i < changeElementsArray.length; i++) {
        changeElementsArrayChildren.push(changeElementsArray[i].firstChild);
      }
      this.openDiaryModal();
      this.getAndSetAllEmotions();
    }, 0)
  }

  nextMonth() {
    let element: HTMLButtonElement = document.getElementsByClassName("mat-calendar-next-button")[0] as HTMLButtonElement;
    element.click();
    setTimeout(() => {
      this.currentMonth = document.getElementsByClassName('mat-calendar-body-cell')[0].getAttribute('aria-label').toString();
      let split = this.currentMonth.split(" ");
      this.currentMonth = split[0] + " " + split[2];
      let changeElementsArray = document.getElementsByClassName('mat-calendar-body-cell');
      let changeElementsArrayChildren = [];
      for (let i = 0; i < changeElementsArray.length; i++) {
        changeElementsArrayChildren.push(changeElementsArray[i].firstChild);
      }
      this.openDiaryModal();
      this.getAndSetAllEmotions();
    }, 0)
  }

  getDefaultEmotions() {
    this.db.getEmotionsFromComboEmotions().then(arr => {
      this.moodColours = [];
      for (let i = 0; i < arr.length; i++) {
        if (this.moodColours.length < 8) {
          console.log('Emotions: ' + arr[i].emotion + ' ' + arr[i].colour);
          this.moodColours.push([{ mood: arr[i].emotion, hex: arr[i].colour }]);
        }
      }
    });
  }

  colourClick(mood: String) {
    console.log(mood);
    if (mood == "Go Back") {
      this.getDefaultEmotions();
    } else {
      this.currentlyClicked = mood;
      this.db.getMoodComboFromDB(mood.toLowerCase()).then(res => {
        if (res.length == 0) {
          this.getDefaultEmotions();
        } else {
          this.moodColours = [];
          this.moodColours.push([{ mood: "Go Back", hex: "#000000" }]);
          if (res[0].comboEmotion1 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion1, hex: res[0].comboEmotionColour1 }]);
          }
          if (res[0].comboEmotion2 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion2, hex: res[0].comboEmotionColour2 }]);
          }
          if (res[0].comboEmotion3 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion3, hex: res[0].comboEmotionColour3 }]);
          }
          if (res[0].comboEmotion4 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion4, hex: res[0].comboEmotionColour4 }]);
          }
          if (res[0].comboEmotion5 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion5, hex: res[0].comboEmotionColour5 }]);
          }
          if (res[0].comboEmotion6 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion6, hex: res[0].comboEmotionColour6 }]);
          }
          if (res[0].comboEmotion7 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion7, hex: res[0].comboEmotionColour7 }]);
          }
          if (res[0].comboEmotion8 !== 'none') {
            this.moodColours.push([{ mood: res[0].comboEmotion8, hex: res[0].comboEmotionColour8 }]);
          }
          console.log(this.moodColours);
        }
      });
    }

  }

}
