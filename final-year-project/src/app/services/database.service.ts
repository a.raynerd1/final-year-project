import { Platform, AngularDelegate } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Emotions {
  date: string,
  emotions: string
}

export interface Dates {
  date: string,
  textMorning: string,
  textAfternoon: string,
  textEvening: string
}

@Injectable({
  providedIn: 'root'
})

export class DatabaseService {
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  emotions = new BehaviorSubject([]);
  dates = new BehaviorSubject([]);

  constructor(private plt: Platform, private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient) {
    this.plt.ready().then(() => {
      return this.sqlite.create({
        name: 'emotions.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        this.database = db;
        this.seedDatabase();
        return null;
      }).catch((error: Error) => {
        console.log("Error: " + error);
        return Promise.reject(error.message || error);
      });
    });
  }

  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text' })
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(_ => {
            this.loadEmotions();
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  getEmotion(): Observable<Emotions[]> {
    return this.emotions.asObservable();
  }

  getDates(): Observable<any[]> {
    return this.dates.asObservable();
  }

  loadEmotions() {
    return this.database.executeSql('SELECT * FROM emotions', []).then(data => {
      let emotions: Emotions[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          emotions.push({
            date: data.rows.item(i).date,
            emotions: data.rows.item(i).emotions
          });
        }
      }
      this.emotions.next(emotions);
      for (let i = 0; i < emotions.length; i++) {
        console.log(emotions[i]);
      }
    });
  }


  loadAndReturnAllEmotions(): Promise<Array<Emotions>> {
    return this.database.executeSql('SELECT * FROM emotions', []).then(data => {
      let emotions: Emotions[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          emotions.push({
            date: data.rows.item(i).date,
            emotions: data.rows.item(i).emotions
          });
        }
      }
      return emotions;
    });
  }

  addEmotions(date, emotions) {
    let data = [date, emotions];
    return this.database.executeSql('INSERT INTO emotions (date, emotions) VALUES (?, ?)', data).then(data => {
      this.loadEmotions();
    });
  }



  getEmotions(date: String): Promise<Emotions> {
    return this.database.executeSql('SELECT * FROM emotions WHERE date = ?', [date]).then(data => {
      if (data.rows.item(0) && data.rows.item(0).date != '') {
        date = JSON.parse(data.rows.item(0).date);
      }
      return {
        date: data.rows.item(0).date,
        emotions: data.rows.item(0).emotions
      }
    });
  }

  getEmotionsForSetDate(date: String) {
    return this.database.executeSql('SELECT * FROM emotions WHERE date = ?', [date]).then(data => {
      let emotions: Emotions[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          emotions.push({
            date: data.rows.item(i).date,
            emotions: data.rows.item(i).emotions
          });
        }
      }
      return emotions;
    });
  }

  getAllEmotions(): Promise<any> {
    return this.database.executeSql('SELECT * FROM emotions', []).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i).date, data.rows.item(i).emotions);
      }
      return arr;
    });
  }

  deleteEmotion(date: String) {
    return this.database.executeSql('DELETE FROM emotions WHERE date = ?', [date]).then(_ => {
      this.loadEmotions();
    });
  }

  deleteAllEmotions() {
    return this.database.executeSql('DELETE FROM emotions').then(response => {
      console.log(response);
      this.loadEmotions();
      return response;
    });
  }

  updateEmotion(emotion: Emotions) {
    let data = [emotion.date, emotion.emotions];
    return this.database.executeSql(`UPDATE emotions SET id = ?, date = ?, emotions = ? WHERE date = ${emotion.date}`, data).then(data => {
      this.loadEmotions();
    })
  }

  saveDiary(date: String, diaryMorning: String, diaryAfternoon: String, diaryEvening: String) {
    let data = [date, diaryMorning, diaryAfternoon, diaryEvening];
    return this.database.executeSql('DELETE FROM dates WHERE date = ?', [date]).then(res => {
      return this.database.executeSql('INSERT INTO dates (date, textMorning, textAfternoon, textEvening) VALUES (?, ?, ?, ?)', data).then(data => {
        console.log(data);
      });
    });
  }

  getDiaries() {
    return this.database.executeSql('SELECT * FROM dates', []).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      return arr;
    });
  }

  getDiariesForSpecificMonth(month: string) {
    let arr = month.split(' ');
    let m = arr[0];
    let y = arr[1];
    return this.database.executeSql("SELECT * FROM dates WHERE date LIKE '%" + m + "%' AND date LIKE '%" + y + "%'", []).then(res => {
      let arr = [];
      for (let i = 0; i < res.rows.length; i++) {
        arr.push(res.rows.item(i));
      }
      return arr;
    });
  }

  getDiariesForSingleDate(date: string): Promise<any> {
    return this.database.executeSql('SELECT * FROM dates WHERE date = ?', [date]).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      return arr;
    });
  }

  getDiaryDates(inputText: string): Promise<any> {
    return this.database.executeSql('SELECT * FROM words WHERE word = ' + inputText + "'hello'", []).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      return arr;
    });
  }

  getDiaryDatesForSpecificMonth(inputText: string, date: string): Promise<any> {
    return this.database.executeSql('SELECT * FROM words WHERE word = ' + inputText + "'hello'", []).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i), date);
      }
      return arr;
    });
  }


  getMoodComboFromDB(mainEmotion) {
    return this.database.executeSql('SELECT * FROM comboEmotions WHERE emotion = ?', [mainEmotion]).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      console.log(arr);
      return arr;
    });
  }

  getEmotionForMultipleDates(dates: String[]) {
    let stringBuild = '';
    for (let i = 0; i < dates.length; i++) {
      if (i + 1 == dates.length) {
        stringBuild += "'" + dates[i] + "'";
      } else {
        stringBuild += "'" + dates[i] + "'" + " OR date = ";
      }
    }
    return this.database.executeSql('SELECT * FROM emotions WHERE date = ' + stringBuild, []).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      return arr;
    });
  }

  getEmotionsFromComboEmotions() {
    return this.database.executeSql('SELECT * FROM comboEmotions', []).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      return arr;
    });
  }

  getAllWords(associatedEmotion: String) {
    return this.database.executeSql('SELECT * FROM words WHERE associatedEmotion = ?', [associatedEmotion]).then(data => {
      let arr = [];
      for (let i = 0; i < data.rows.length; i++) {
        arr.push(data.rows.item(i));
      }
      console.log(arr);
      return arr;
    });
  }
}
